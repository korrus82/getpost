﻿package com.kursyjava.getpost;

public interface SupermanInterface {
	public int speed = 600;

	//  в java 1,8 фигурирует так называемый default interface
	public default void flyOverSky() {
		System.out.println("Я полетело");
	}
	
}
