﻿package com.kursyjava.getpost;

import java.io.Serializable;

public class Book implements Serializable {
	
	public static final long serialVersionUID = 20140901;
	private String title;
	private transient int price;

	public Book() {
	}

	public Book(Book book) {
		setTitle(book.getTitle());
		setPrice(book.getPrice());
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}

		if (this == object) {
			return true;
		}

		if (getClass() != object.getClass()) {
			return false;
		}
		Book Book = (Book) object;

		if (getTitle() != null) {
			if (!getTitle().equals(Book.getTitle())) {
				return false;
			}
		} else {
			if (Book.getTitle() != null) {
				return false;
			}
		}

		if (getPrice() != Book.getPrice()) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 1;

		hash = hash * 10 + getPrice();

		if (getTitle() != null) {
			hash = hash * 12 + getTitle().hashCode();
		}

		return hash;
	}

    @Override
    public String toString() {
        return "Book [title=" + title + ", price=" + price + "]";
    }

}
