﻿package com.kursyjava.getpost;

import java.io.Serializable;

public class Programmer extends AbstractWorker implements SupermanInterface, Cloneable, Serializable {
    public static final long serialVersionUID = 20140901;
    private boolean isCodeJava;
    private Book book;

    public Programmer() {
    }

    public Programmer(Programmer firstProgrammer) {
        setFirstName(firstProgrammer.getFirstName());
        setSurname(firstProgrammer.getSurname());
        setLastName(firstProgrammer.getLastName());
        setAge(firstProgrammer.getAge());
        setBook(firstProgrammer.getBook());
        setCodeJava(firstProgrammer.isCodeJava());
    }

    public Programmer(String firstName, String surname, String lastName, int age) {
        super(firstName, surname, lastName, age);
    }

    public Programmer(String firstName, String surname, String lastName, int age, boolean isCodeJava, Book book) {
        super(firstName, surname, lastName, age);
        setCodeJava(isCodeJava);
        setBook(book);
    }

    public void setCodeJava(boolean isCodeJava) {
        this.isCodeJava = isCodeJava;
    }

    public boolean isCodeJava() {
        return isCodeJava;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public void call() {
        System.out.println("Hello im programmer");
    }

    @Override
    public void flyOverSky() {
        System.out.println("Im fly!!!!!!!!!!!");
    }

    @Override
    public int hashCode() {
        // Если хеш-коды разные, то и входные объекты
        // гарантированно разные.
        // Если хеш-коды равны, то входные объекты не всегда равны.
        // Ситуация, когда у разных объектов одинаковые хеш-коды называется —
        // коллизией
        int hash = 19 + super.hashCode();

        if (isCodeJava()) {
            hash = hash * 10 + 100;
        } else {
            hash = hash * 11 + 6;
        }

        if (getBook() != null) {
            hash = hash * 15 + getBook().hashCode();
        }

        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }

        if (this == object) {
            return true;
        }

        if (getClass() != object.getClass()) {
            return false;
        }
        Programmer programmer = (Programmer) object;

        if (!super.equals(programmer)) {
            return false;
        }

        if (getBook() != null) {
            if (!getBook().equals(programmer.getBook())) {
                return false;
            }
        } else {
            if (programmer.getBook() != null) {
                return false;
            }
        }

        if (isCodeJava() != programmer.isCodeJava()) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "Programmer [isCodeJava=" + isCodeJava + ", book=" + book.toString() + ", getFirstName()=" + getFirstName() + ", getSurname()=" + getSurname()
                + ", getLastName()=" + getLastName() + ", getAge()=" + getAge() + "]";
//      return "Programmer [getFirstName()=" + getFirstName()  + "]";
    }

    @Override
    public Programmer clone() {
        Programmer clone = null;
        try {
            clone = (Programmer) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        clone.setBook(new Book(getBook()));

        return clone;
    }
}
