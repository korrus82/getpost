﻿package com.kursyjava.getpost;

import java.io.Serializable;

public abstract class AbstractWorker extends Object implements Serializable, Comparable<AbstractWorker> {
    public static final long serialVersionUID = 20140901;
    private String firstName;
    private String surname;
    private String lastName;
    private int age;

    // конструктор по умолчанию
    public AbstractWorker() {
        age = 10;
    }

    public AbstractWorker(String firstName, String surname, String lastName, int age) {
        this.firstName = firstName;
        this.surname = surname;
        this.lastName = lastName;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setAge(String age) {
        this.age = Integer.parseInt(age);
    }

    public abstract void call();

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }

        if (this == object) {
            return true;
        }

        if (getClass() != object.getClass()) {
            return false;
        }
        AbstractWorker abstractWorker = (AbstractWorker) object;

        if (getFirstName() != null) {
            if (!getFirstName().equals(abstractWorker.getFirstName())) {
                return false;
            }
        } else {
            if (abstractWorker.getFirstName() != null) {
                return false;
            }
        }

        if (getSurname() != null) {
            if (!getSurname().equals(abstractWorker.getSurname())) {
                return false;
            }
        } else {
            if (abstractWorker.getSurname() != null) {
                return false;
            }
        }

        if (getLastName() != null) {
            if (!getLastName().equals(abstractWorker.getLastName())) {
                return false;
            }
        } else {
            if (abstractWorker.getLastName() != null) {
                return false;
            }
        }

        if (getAge() != abstractWorker.getAge()) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 1;

        if (getFirstName() != null) {
            hash = hash * 13 + getFirstName().hashCode();
        }

        if (getSurname() != null) {
            hash = hash * 19 + getSurname().hashCode();
        }

        if (getLastName() != null) {
            hash = hash * 5 + getLastName().hashCode();
        }

        hash = hash * 2 + getAge();

        return hash;
    }

    @Override
    public int compareTo(AbstractWorker abstractWorker) {
        if (equals(abstractWorker)) {
            return 0;
        }

        int compareTo = getFirstName().compareToIgnoreCase(abstractWorker.getFirstName());
        if (compareTo == 0) {
            return -1;
        }

        return compareTo;
    }

    // public Comparator<AbstractWorker> lastNameComparator = new
    // Comparator<AbstractWorker>() {
    //
    // public int compare(AbstractWorker abstractWorker1, AbstractWorker
    // abstractWorker2) {
    // // сортируем по возрастанию
    // return
    // abstractWorker1.getLastName().compareTo(abstractWorker2.getLastName());
    //
    // // сортируем по убыванию
    // // return
    // //
    // abstractWorker2.getLastName().compareTo(abstractWorker1.getLastName());
    // }
    // };

    @Override
    public String toString() {
        return getFirstName() + " " + getSurname();
    }

}
