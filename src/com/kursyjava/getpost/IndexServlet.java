package com.kursyjava.getpost;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/index.html")
public class IndexServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Book book1 = new Book();
        book1.setPrice(1000);
        book1.setTitle("thinking java");

        List<Programmer> programmersList = new ArrayList<>();
        programmersList.add(new Programmer("���������", "�����������", "�������", 20, true, book1));
        programmersList.add(new Programmer("���������", "�����������", "�����", 20, true, book1));
        programmersList.add(new Programmer("�������", "����������", "��������", 21, true, book1));
        programmersList.add(new Programmer("������", "�����������", "������������", 22, true, book1));
        programmersList.add(new Programmer("����", "��������", "�������", 23, true, book1));
        programmersList.add(new Programmer("�������", "�����������", "��������", 24, true, book1));
        programmersList.add(new Programmer("����������", "��������", "������", 25, true, book1));
        programmersList.add(new Programmer("������", "����������", "�����", 26, true, book1));
        programmersList.add(new Programmer("�������", "����������", "������", 27, true, book1));

        request.setAttribute("programmersList", programmersList);
        
        request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        request.setAttribute("login", login);
        request.setAttribute("password", password);

        request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
    }

}
