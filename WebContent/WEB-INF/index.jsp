<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Insert title here</title>
	</head>
	<body>
		<h1>Hello world !</h1>
		${login}
		${password}
		
		<c:forEach var="each" items="${programmersList}">
			${each.getFirstName()} ${each.getSurname()} <br>
		</c:forEach>
		
		<form method="post">
			<input type="text" name="login">
			<input type="password" name="password">
			<input type="submit">
		</form>
	</body>
</html>